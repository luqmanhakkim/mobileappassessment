import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todoapp/create_todo.dart';
import 'package:todoapp/model/todo.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool checkValue = false;
  final List<Todo> todos = [];
  var customFormat = DateFormat('dd-MM-yyyy');

  ///Function below states that all list in the model being added
  addList({Todo todo}) {
    setState(() {
      todos.add(todo);
    });
  }

  Widget itemCard(Todo todo) {
    void toggleCheckbox(bool value) {
      setState(() {
        checkValue = value;
        todo.isDone = checkValue;
      });
    }

    /// Difference method to compare  between two days
    final difference = DateTime.parse(todo.endDate)
        .difference(DateTime.parse(todo.startDate))
        .inDays;

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20),
            child: Text(todo?.title.toString(),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          ),
          Padding(padding: EdgeInsets.all(10)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Start Date',
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              Text('End Date',
                  style: TextStyle(fontSize: 12, color: Colors.grey)),
              Text('Time Left',
                  style: TextStyle(fontSize: 12, color: Colors.grey)),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(5),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                customFormat.format(DateTime.parse(todo.startDate)),
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              ),
              Text(customFormat.format(DateTime.parse(todo.endDate)),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
              Text('$difference',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
              Padding(
                padding: EdgeInsets.only(right: 10),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.all(10)),
          Container(
              padding: EdgeInsets.only(left: 20),
              color: Color.fromRGBO(231, 227, 210, 1),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Text('Status',
                            style: TextStyle(color: Colors.grey, fontSize: 12)),
                      ),
                      Text(todo.isDone == true ? 'Completed' : 'Incompleted',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold)),
                      Padding(
                        padding: const EdgeInsets.only(left: 18),
                        child: Text('Tick if completed',
                            style: TextStyle(fontSize: 12, color: Colors.grey)),
                      ),
                      Checkbox(
                        value: todo.isDone,
                        onChanged: toggleCheckbox,
                        activeColor: Color.fromRGBO(241, 187, 65, 1),
                        tristate: false,
                      ),
                    ],
                  ),
                ],
              )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 242, 242, 1),
      appBar: AppBar(
        title: Text(widget.title, textAlign: TextAlign.end),
        backgroundColor: Color.fromRGBO(241, 187, 65, 1),
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: ListView.builder(
            itemCount: todos.length,
            itemBuilder: (BuildContext context, int index) {
              final todo = todos[index];

              deleteList({Todo todo}) {
                setState(() {
                  todos.remove(todo);
                });
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(10)),
                  Dismissible(
                    key: Key('${todo.hashCode}'),
                    child: itemCard(todo),
                    background: Container(
                        margin: EdgeInsets.all(40),
                        padding: EdgeInsets.all(20),
                        color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.delete, color: Colors.white),
                            Text(
                              'Delete',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                    onDismissed: (direction) {
                      setState(() {
                        deleteList(todo: todo);
                      });
                    },
                  ),
                ],
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreateTodo(addList: addList))),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
