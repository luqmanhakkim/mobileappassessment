class Todo {
  Todo({this.title, this.isDone = false, this.startDate, this.endDate});

  String title;
  bool isDone;
  String startDate;
  String endDate;
}
