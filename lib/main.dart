import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:todoapp/homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ToDo App',
      home: MyHomePage(title: 'To-Do List'),
    );
  }
}

