import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateField extends StatefulWidget {
  final Function(DateTime date) onSelectDate;
  final String hint;

  const DateField({
    this.onSelectDate,
    this.hint,
  });

  @override
  _DateFieldState createState() => _DateFieldState();
}

class _DateFieldState extends State<DateField> {
  DateTime selectedDate;
  var customFormat = DateFormat('dd-MM-yyyy');

  /// DatePicker method
  Future<DateTime> showPicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1990),
        lastDate: DateTime(2100));

    return picked;
  }

  endDateSet() {
    setState(() => customFormat.format(selectedDate));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.black),
      ),
      child: InkWell(
        onTap: () async {
          DateTime picked = await showPicker(context);
          if (picked != null && picked != selectedDate) {
            setState(() {
              selectedDate = picked;
            });
            widget.onSelectDate(selectedDate);
          }
        },
        child: ListTile(
          trailing: Icon(Icons.arrow_drop_down),
          title: Text(
            selectedDate == null
                ? widget.hint
                : customFormat.format(selectedDate),
            style: TextStyle(
                color: selectedDate == null ? Colors.grey : Colors.black),
          ),
        ),
      ),
    );
  }
}
